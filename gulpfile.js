// https://github.com/violentmonkey/violentmonkey/blob/master/gulpfile.js

const fs = require('fs').promises;
const gulp = require('gulp');
const del = require('del');
const compiler = require('webpack');
const webpack = require('webpack-stream');

const { buildManifest } = require('./scripts/manifest.js');
const webpackConf = require('./src/webpack.config.js');

const isDevelopment = process.env.NODE_ENV !== 'production';

const DIST = 'dist';
const paths = {
	manifest: 'src/manifest.json',
	src: [
		'src/**/*.js',
		'src/**/*.html',
		'src/**/*.scss',
	]
}

function clean() {
	return del(DIST);
}

function watch() {
	gulp.watch(paths.manifest, manifest);
	gulp.watch(paths.src, build);
}

function copyImages() {
	return gulp.src('assets/*')
		.pipe(gulp.dest(`${DIST}/assets`))
}

function build() {
	return gulp.src('src/injected/entry.js')
		.pipe(webpack(webpackConf, compiler))
		.pipe(gulp.dest(`${DIST}/`))
}

async function manifest() {
	const data = buildManifest();
	await fs.mkdir(DIST).catch(() => {});
	if (isDevelopment) {
		await fs.writeFile(`${DIST}/manifest.json`, JSON.stringify(data, null, 4), 'utf8');
	} else {
		await fs.writeFile(`${DIST}/manifest.json`, JSON.stringify(data), 'utf8');
	}
}

module.exports = {
	clean,
	manifest,

	dev: gulp.series(manifest, gulp.parallel(copyImages, build), watch),
	build: gulp.series(clean, manifest, gulp.parallel(copyImages, build))
}
