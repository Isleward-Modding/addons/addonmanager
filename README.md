<!--

References:

chrome://extensions/
violentmonkey/tampermonkey extensions
https://wowup.io/
https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Build_a_cross_browser_extension
https://developer.chrome.com/docs/extensions/mv3/messaging/
https://developer.chrome.com/docs/webstore/publish/
https://github.com/greasemonkey/greasemonkey/blob/master/src/user-script-obj.js#L319
https://github.com/violentmonkey/violentmonkey
https://github.com/samuelsimoes/chrome-extension-webpack-boilerplate/blob/master/webpack.config.js#L85
https://adamlynch.com/flexible-data-tables-with-css-grid/
https://recoiljs.org/docs/guides/asynchronous-data-queries/
https://betterprogramming.pub/implementing-list-items-with-react-and-recoil-c58f75e307dc
https://blog.appsignal.com/2021/08/25/exploring-asynchronous-requests-in-recoil.html

-->
# Isleward Addon Manager

Addon Manager for Isleward.
Provides addon management with enable/disable, browse, updating, and hot reloading (for addon development).

## Usage

Eventually there will probably be a Chrome extension published (and for other browsers?).

To use it now:

* Clone
* `yarn && yarn build`
* Load unpacked extension from `dist/`

Click the extension icon to open the extension options.

## Goals

* Loading and storing of addons (manually, from url (TBA), from file, from online repository)
* Fetch updates and prompt the user to update addons
* Toggling of addons (+ "hot" enable/disable/reload support for addons that support it)
* Cross-compatibility with non-AddonManager environments (read more below)
* Page/popup for managing addons fast (TBA)

## Todo

* URL request shouldn't be synchronous?
* Toggle filesystem addons without removing them
* Add from url (copy content, don't refetch)
* Notify tabs when addons are enabled/disabled in options/popup/etc
* Automatic update checks
* Check all/update all
* New icon
* Popup like tampermonkey/violentmonkey? maybe it can be injected into the iwd window instead of a popup/new window?
* Addon sets/profiles?
* Addon preferences storage? (probably not, diverges from non-AM env)

## Contributing

### Addon Development

If you write an addon for Isleward, you can support hot-reloading and hot-toggling for your addon by registering through the Addon Manager.
Hot-reload enables faster development since you can reload your code without reloading Isleward and logging back in.
Your addon will need a `cleanup()` method to remove any listeners/UIs/etc so the addon can be fully disabled, and so a newer version of the addon can be loaded.

You can use this code in your addon file to allow non-AddonManager users (Tampermonkey, Violentmonkey, Electron client, etc)
that load the js file plainly (without the AddonManager global) to be able to use your addon the same, just without hot-reload/hot-toggle.
The function will wait for AM or IWD addons to be ready, and will register the addon object passed to it.

```js
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'example' : ADDON_ID,
	name: 'Example',

	init: function () {
		// window.addons.events is guaranteed to be available now
		console.log('Hello World!');
	},

	cleanup: function () {
		// Do stuff, if using AddonManager
	}
});
```

This snippet will check if AddonManager is available, and will then wait for AddonManager or `window.addons` to be ready, and then register the addon.

The name is only used for logging.
The ID is only used for hot-reloading and should be unique (2 addons with the same ID won't both be loaded). If left out, hot-reloading won't work.
You should use the `ADDON_ID` variable (if present) in case the ID set in the AddonManager UI is somehow different from the hard-coded one.
If the user-configured ID and code ID don't match, hot-reloads will probably break.
The presence or absence of either property won't affect plainly-loaded addons.

More examples of AddonManager-supporting addons are in `addons/`.

**Note:** You can't depend on `cleanup()`.
It is only used when the addon will be loaded again in the same page session (when the addon is disabled or reloaded).
It won't run when the page is refreshed/closed.
It's mostly intended for hot reloading for fast local development of addons.
If AddonManager is used and `cleanup` is not defined on your addon, AM will not attempt to load it again when hot-reloading.

### Repositories

Addons can be submitted to one of the lists in `repositories/` to be found in the browse list in the Addon Manager.
Please open an issue or merge request if you have submissions.
Users can also add their own repository lists to the Addon Manager to browse other collections of addons.

### Compatibility

The snippet above should work to load your addon in both environments.
Additionally, you should include a minimal userscript header to make sure userscript extensions like VioletMonkey can load your addon.

This Addon Manager can also load plain userscripts, but won't be able to properly remove them when hot-reloading (a full refresh will always be necessary).
Userscripts that are listed in the Addon Manager but don't register with `AddonManager.register`, or that don't have a cleanup method, will not be hot reloaded.

Note that the addon manager doesn't handle all userscript headers, so headers for `include`/`require`/`grant`/`match`/etc will likely not do anything.
If you need these headers for an addon that depends strongly on them, use VioletMonkey instead.
You can avoid needing `include`/`require` headers by bundling your addon using something like Webpack (examples are in `addons/`).
AM already only runs on IWD live and PTR, so `match` is not needed either.
`GM_` functions are not supported.

It's up to you to get your addon to work in both environments, but it should be pretty simple.
Plain IWD addons only support `init()`, so you can't rely on any other AddonManager methods to be available.
If you do, you should detect the presence of AddonManager.

If your addon is running in AM, `ADDON_MANAGER` will be set to `AddonManager`.
Otherwise it will be undefined **and** undeclared, so you should check with typeof: `typeof ADDON_MANAGER === 'undefined' ? /* not supported */ : /* supported */`.

### Development

Issues and MRs are welcome!

* Clone
* `yarn`
* `yarn dev`
* Load unpacked extension from `dist/`
* `Ctrl+M` reloads (should probably be disabled in production?)

### Production

* `yarn build`

## Thanks

TBA
