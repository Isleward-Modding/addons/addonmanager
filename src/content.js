// This content script is executed by the extension manifest on Isleward page loads
// The content script adds the actual addon manager script to the page since it needs access to the Isleward page's window

(async () => {
	let lastId = 0;

	let unlockIframe;
	const iframeLock = new Promise((resolve) => {
		unlockIframe = () => resolve();
	});
	const iframeOrigin = chrome.runtime.getURL('iframe.html');

	// Send message to background worker to fetch the addon list and addon code
	async function getEnabledAddons() {
		return new Promise((resolve) => {
			chrome.runtime.sendMessage({ request: 'getEnabledAddons' }, (data) => {
				resolve(data);
			});
		});
	}

	// Send message to injected iframe to read addons from filesystem
	async function getFileAddons(localList) {
		const id = lastId++;

		return new Promise((resolve) => {
			const listener = function (e) {
				if (e?.data?.type !== 'iwdam_respondFileAddons') return;
				if (e?.data?.detail?.requestId === id) {
					// Deregister self
					window.document.removeEventListener('message', listener);
					resolve(e?.data?.detail?.data);
				}
			}
			window.addEventListener('message', listener);

			iframeEl.contentWindow.postMessage({ type: 'iwdam_requestFileAddons', detail: { id, origin: window.location.origin, localList } }, iframeOrigin);
		});
	}

	// Listen to requests from injected script to background script
	async function onRequestEnabledAddons(e) {
		const request = e.detail;

		let { enabled, localList } = await getEnabledAddons();

		if (localList.length > 0) {
			enabled = [...enabled, ...(await getFileAddons(localList))];
		}

		const response = {
			requestId: request.id,
			data: enabled
		}
		const responseEvent = new CustomEvent('iwdam_respondEnabledAddons', { detail: response });
		window.dispatchEvent(responseEvent);
	}
	window.addEventListener('iwdam_requestEnabledAddons', onRequestEnabledAddons);

	// Inject iframe (for local files)
	let iframeEl = document.createElement('iframe');
	iframeEl.style.display = 'none';
	iframeEl.src = iframeOrigin;
	iframeEl.onload = () => {
		unlockIframe();
	}
	document.documentElement.appendChild(iframeEl);

	// Wait for iframe to be loaded
	await iframeLock;

	// Inject addon loader
	let scriptEl = document.createElement('script');
	scriptEl.src = chrome.runtime.getURL('injected.js');
	scriptEl.onload = function () {
		// maybe do some stuff after the script is loaded
	}
	document.documentElement.appendChild(scriptEl);
})();
