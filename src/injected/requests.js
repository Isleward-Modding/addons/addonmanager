let requestId = 0;

// https://stackoverflow.com/questions/33063774/communication-from-an-injected-script-to-the-content-script-with-a-response
const requestEnabledAddons = () => {
	let id = requestId++;

	return new Promise((resolve) => {
		let listener = function (e) {
			if (e.detail.requestId === id) {
				// Deregister self
				window.removeEventListener('iwdam_respondEnabledAddons', listener);
				resolve(e.detail.data);
			}
		}
		window.addEventListener('iwdam_respondEnabledAddons', listener);

		let reqEvent = new CustomEvent('iwdam_requestEnabledAddons', { detail: { id } });
		window.dispatchEvent(reqEvent);
	});
}

export { requestEnabledAddons }
