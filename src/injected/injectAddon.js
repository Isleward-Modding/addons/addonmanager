let scriptElements = [];

const injectAddon = (metadata) => {
	const scriptContent = generateScriptContent(metadata);

	const scriptEl = document.createElement('script');
	const inlineScriptEl = document.createTextNode(scriptContent);
	scriptEl.appendChild(inlineScriptEl);

	document.body.appendChild(scriptEl);

	scriptElements.push({ id: metadata.id, el: scriptEl });
}

// See GreaseMonkey's calculateEvalContent() implementation
// https://github.com/greasemonkey/greasemonkey/blob/efd22a93121225ada47f2fe9f021af0ab6100c21/src/user-script-obj.js#L319
const generateScriptContent = (metadata) => {
	return `try { (function scopeWrapper() { function userScript() { ${metadata.raw}
}
/* Injected variables */
const ADDON_MANAGER = 'AddonManager';
const ADDON_ID = ${JSON.stringify(metadata.id)};
userScript();
})();
} catch (err) {
	console.error('Error while executing addon script for "${metadata.name}":\\n', err);
}`;
}

const cleanupAddonScripts = (skipList) => {
	scriptElements = scriptElements.filter((entry) => {
		let { id, el } = entry;
		let shouldKeep = skipList.includes(id);

		if (!shouldKeep) {
			el.parentNode.removeChild(el);
		}

		return shouldKeep;
	});
}

export { injectAddon, cleanupAddonScripts }
