/// Prefixed log helper functions

// TODO: store errors/warnings somewhere?

const logInfo = (...args) => console.log('[AM]', ...args);
const logWarn = (...args) => console.warn('[AM]', ...args);
const logError = (...args) => console.error('[AM]', ...args);
const log = (tags, ...args) => {
	if (!Array.isArray(tags)) {
		tags = [tags];
	}

	// Custom [tag] prefixes
	const tagString = tags
		.filter(t => t != 'error' && t != 'warn')
		.map(t => `[${t}]`)
		.join('');

	if (tagString != '') {
		args.unshift(tagString);
	}

	if (tags.includes('error')) {
		logError(...args);
	} else if (tags.includes('warn')) {
		logWarn(...args);
	} else {
		logInfo(...args);
	}
}

export {
	logInfo,
	logWarn,
	logError,
	log
}
