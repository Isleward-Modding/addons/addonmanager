// Imports
import { logInfo, logError } from './logging.js';
import { injectAddon, cleanupAddonScripts } from './injectAddon.js';
import { requestEnabledAddons } from './requests.js';

// Provide API for addons
import { AddonManager, addonList, cleanupAddonList, setInitAddonSafely, setInitRun } from './api.js';
window.AddonManager = AddonManager;
window.AM_log = AddonManager.log; // deprecated

// For enabling/disabling debug features
const DEBUG = false;

// Store enabled list
let enabledList = [];
let skipList = [];

setInitRun(false);
setInitAddonSafely(initAddonSafely);

// Run this after all addons are injected & registered
// This function will either init addons immediately (hot reload), or wait for IWD to be ready for addons
function maybeWaitForIsleward() {
	// If iwd app is already set up (hot reload), load now
	if (window.addons && window.addons.events) {
		afterIslewardReady();
	} else {
		// Otherwise, wait a bit
		waitForIsleward();
	}
}

// Hook IWD addons module to wait for IWD to be ready for addons to listen to events
function waitForIsleward() {
	if (window.addons) {
		loadViaAddon();
	} else {
		setTimeout(waitForIsleward, 15);
	}
}
function loadViaAddon() {
	window.addons.register({
		init: () => {
			afterIslewardReady();
		}
	});
}

// Called when Isleward is ready (addons and events are available)
function afterIslewardReady() {
	if (DEBUG) console.log('Scripts injected');

	// todo: context for addons?

	// Call addon init methods safely
	// Don't re-init non-reloadable scripts
	addonList.forEach(a => {
		initAddonSafely(a);
	});

	setInitRun(true);
}

// Helper
function initAddonSafely(a) {
	if (a.enabled && a.init && !skipList.includes(a.id)) {
		try {
			a.init();
		} catch (e) {
			// Don't update
			a.enabled = false;

			logError(`Error while initializing addon "${a.name}" (addon has been disabled, please contact the addon's maintainer!)`);
			console.error(e);
		}
	}
}

// Call addon cleanup methods safely
function cleanupAddonSafely(a) {
	if (a.enabled && a.cleanup) {
		try {
			a.cleanup();
		} catch (e) {
			logError(`Error while cleaning up addon "${a.name}" (please contact the addon's maintainer!)`);
			console.error(e);
		}
	}
}

// Cleanup everything (remove all listeners)
function cleanup(data) {
	if (DEBUG) console.log('Cleaning', data);

	// Clean up addons
	addonList.forEach(a => {
		cleanupAddonSafely(a);
	});

	enabledList.forEach(a => {
		// If the addon wasn't registered with us or the addon has no cleanup
		let found = addonList.find(b => b.id === a.id);
		if (!found || !found.cleanup) {
			// And if the skipList doesn't already have this addon
			if (!skipList.includes(a.id)) {
				skipList.push(a.id);
			}
		}
	});

	// Remove injected addon scripts
	cleanupAddonScripts(skipList);

	// Reset addon list
	cleanupAddonList(skipList);

	setInitRun(false);
}

// Fetch and inject addon scripts
// Injected addons should do AddonManager.register({ ... }) to register
async function injectAddons() {
	// Injected -> Content -> Background -> Content -> Injected
	// enabledList will be an array of metadata objects with a .raw property for the actual code
	enabledList = await requestEnabledAddons();

	await Promise.all(
		enabledList
			.filter(m => !skipList.includes(m.id))
			.map(m => injectAddon(m))
	);
}

// Load addons
async function load() {
	if (DEBUG) console.log('Loading Addon Manager');

	setInitRun(false);

	// Fetch & inject enabled scripts
	await injectAddons();

	// Log
	const addonLen = addonList.length;
	if (addonLen) {
		logInfo(`Loaded ${addonLen} addon${addonLen != 1 ? 's' : ''}:`, addonList.map(m => m.name).sort().join(', '))
	} else {
		logInfo(`No addons loaded`);
	}

	// Wait for isleward and then init addons
	maybeWaitForIsleward();
}

// Listen for hot reloads
function onDocumentKeyDown(e) {
	if (e.ctrlKey && e.key === 'm') {
		if (DEBUG) console.log('Reload triggered');

		// Reload
		cleanup();
		load();
	}
}
document.addEventListener('keydown', onDocumentKeyDown);

// Start
load();
