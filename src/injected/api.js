/// Public object for addons to register with

// Dependencies
import { log, logWarn } from './logging';

// All registered addons
const addonList = [];

// Fallback ID counter
let fallbackIndex = 1;

// Reset
function cleanupAddonList(skipList) {
	for (let i = 0; i < addonList.length; i++) {
		let addon = addonList[i];

		if (!skipList.includes(addon.id)) {
			addonList.splice(i, 1);
			i--;
		}
	}
}

// gross hack for late registers
let ias;
function setInitAddonSafely(fn) {
	ias = fn;
}
let initRun;
function setInitRun(v) {
	initRun = v;
}

const AddonManager = {
	// Addon entry register functin
	register: function (addon) {
		// TODO
		addon.enabled = true;

		// Check for missing IDs
		if (addon.id === undefined || addon.name === null) {
			addon.id = `addon${fallbackIndex}`;
			fallbackIndex += 1;

			logWarn(`An addon was registered without an ID (defaulted to "${addon.id}"). Some features may not work correctly.`);
		}

		// Default display names
		if (addon.name === undefined || addon.name === null) {
			addon.name = addon.id;

			// TODO: this type of low-level dumb checking warn should get saved somewhere else (not console)?
			logWarn(`An addon was registered without a name (was renamed to "${addon.name}").`);
		}

		// Default subdomains to match
		if (addon.match === undefined || addon.match === null) {
			addon.match = ['play', 'ptr', 'race'];
		}
		// Check if the current subdomain is matched
		const subdomain = window.location.host.split('.')[0];
		if (!addon.match.includes(subdomain)) {
			// TODO clarify the reason why this addon was disabled
			addon.disabled = true;
		}

		// Duplicate checking
		const dupe = addonList.find(a => a.id === addon.id);
		if (dupe) {
			logWarn(`Skipped registering potential duplicate addon "${addon.id}"`);
			return;
		}

		// Hack for late registers
		if (initRun) {
			ias(addon);
		} else {
			addonList.push(addon);
		}
	},

	// Utilities
	require: function () {
		throw new Error('AddonManager.require is deprecated')
	},

	log: function (...args) {
		console.warn('[AddonManager][deprecation] AM.log should probably not be used');
		log(...args);
	},
}

export { AddonManager, addonList, cleanupAddonList, setInitAddonSafely, setInitRun };
