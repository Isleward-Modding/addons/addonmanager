// This iframe is responsible for fetching local file:// userscripts
// This is necessary because the background script is a service worker (no XMLHttpRequest) and the Fetch API (XHR replacement) doesn't support file:// URLs
// Since the iframe is a web accessible resource of the extension, it has the permissions of the extension and can read local files (if granted)
// See https://stackoverflow.com/questions/66245298/chrome-extension-how-to-access-local-file-with-manifest-v3

const loadLocalUserscript = (url) => {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.onload = () => {
			if (xhr.status > 300) return reject();

			let name = new URL(url).pathname.split(/[/\\]/);
			name = name[name.length - 1].replace('.user.js', '');
			resolve({
				id: `${name}-${url}`,
				name,
				version: '0.0.0',
				enabled: true,
				raw: xhr.responseText
			});
		}
		xhr.onerror = () => {
			reject();
		}
		xhr.send();
	})
}

async function onRequestFileAddons(e) {
	const request = e.detail;

	const data = [];
	await Promise.all((request.localList ?? []).map(async (u) => {
		try {
			const localAddon = await loadLocalUserscript(u);
			data.push(localAddon);
		} catch(e) {
			console.error('Failed to load URL:', u);
		}
	}));

	const response = {
		type: 'iwdam_respondFileAddons',
		detail: {
			requestId: request.id,
			data
		}
	}
	window.parent.postMessage(response, request.origin);
}

window.addEventListener('message', (e) => {
	if (e.data.type === 'iwdam_requestFileAddons') {
		onRequestFileAddons(e.data);
	}
});
