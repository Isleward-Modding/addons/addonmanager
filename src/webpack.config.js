/* eslint-disable */

const path = require('path');
const { DefinePlugin } = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { getVersion } = require('../scripts/version');
const APPVERSION = getVersion();
const isDevelopment = process.env.NODE_ENV !== 'production';

const config = {
	mode: process.env.NODE_ENV || 'development',
	entry: {
		content: path.join(__dirname, 'content.js'),
		background: path.join(__dirname, 'background', 'background.js'),
		injected: path.join(__dirname, 'injected', 'entry.js'),
		'options/options': path.join(__dirname, 'options', 'options.js'),
		iframe: path.join(__dirname, 'iframe', 'iframe.js'),
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.(sc|sa|c)ss$/i,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: [
									path.resolve(__dirname, 'styles'),
								]
							}
						}
					}
				]
			}
		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
	plugins: [
		new DefinePlugin({
			// Need to stringify it to insert "APPVERSION" (with quotes)
			__APPVERSION__: JSON.stringify(APPVERSION),
		}),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'options', 'options.html'),
			filename: 'options/index.html',
			chunks: ['options/options'],
		}),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'iframe', 'iframe.html'),
			filename: 'iframe.html',
			chunks: ['iframe'],
		}),
	]
}

if (isDevelopment) {
	// Source map for development
	config.devtool = 'inline-source-map';
} else {
	// Minify
	config.optimization = {
		minimize: true,
		minimizer: [ new TerserPlugin() ]
	}
}

module.exports = config;