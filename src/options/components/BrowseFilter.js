import React from 'react';
import { Button, Input } from '@kckckc/isleward-util';

import { useRepos } from '../useRepos';

const maybePlural = (n) => n != 1 ? 's' : '';
const maybePluraly = (n) => n != 1 ? 'ies' : 'y';

const BrowseFilter = ({ setPage, search, setSearch }) => {
	const { fetchedLists } = useRepos();

	const workingCount = fetchedLists.filter(a => !!a.list).length;
	const totalCount = fetchedLists.length;
	const addonCount = fetchedLists.map(r => (r.list ?? [])).flat().length;

	return (
		<div className="addon-filter">
			<div className="addon-filter__row">
				<div className="addon-filter__info">
					<span className="addon-filter__info--bright">{workingCount}</span>
					/
					<span className="addon-filter__info--bright">{totalCount}</span>
					{` repositor${maybePluraly(totalCount)} working`}
					{" "}-{" "}
					<span className="addon-filter__info--bright">
						{addonCount} addon{maybePlural(addonCount)} found
					</span>
				</div>
			</div>

			<div className="addon-filter__row">
				<Input
					value={search}
					setValue={setSearch}
					placeholder="Search available addons..."
				/>

				<div className="addon-filter__action-buttons">
					<Button onClick={() => setPage('settings')}>Manage Repositories</Button>
				</div>
			</div>
		</div>
	);
}

export { BrowseFilter }
