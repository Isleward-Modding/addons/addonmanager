import React from 'react';

const NavigationButton = ({ onClick, isActive, children }) => {
	const onKeyDown = (e) => {
		if (e.key === 'Enter' && onClick) {
			onClick();
		}
	}
	
	return (
		<div
			className={`navigation__button${isActive ? ' navigation__button--active' : ''}`}
			onClick={onClick}
			onKeyDown={onKeyDown}
			tabIndex={0}
		>
			{children}
		</div>
	);
}

const Navigation = ({ page, setPage }) => {
	const btn = (p, c) => (
		<NavigationButton
			isActive={page === p}
			onClick={() => setPage(p)}
		>
			{c}
		</NavigationButton>
	);

	return (
		<div className="navigation">
			<div
				className="navigation__title"
				onClick={() => setPage('home')}
			>
				Isleward Addon Manager
			</div>
			
			<div className="navigation__buttons">
				{btn('home', 'Addons')}
				
				<div className="navigation__separator" />
				
				{btn('browse', 'Browse')}
				
				<div className="navigation__separator" />
				
				{btn('settings', 'Settings')}
			</div>
		</div>
	)
}

export { Navigation }
