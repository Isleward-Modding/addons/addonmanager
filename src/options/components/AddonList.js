import React, { useEffect, useState } from 'react';
import semver from 'semver';
import { Button, Checkbox } from '@kckckc/isleward-util';

import { fetchUserscript, removeAddon, installUserscript } from '../requests';
import { useAddonList, useAddonItem } from '../useAddons';

import './AddonList.scss';

export function MaybeSourceLink({ addon }) {
	const text = addon.source ?? '?';

	if (addon.sourceURL) {
		return <td><a href={addon.sourceURL}>{text}</a></td>
	} else {
		return <td>{text}</td>
	}
}
export function MaybeHomepageLink({ addon }) {
	const link = bestHomepageLink(addon);
	const text = addon.author ?? '?';
	if (link) {
		return <td><a href={link}>{text}</a></td>
	} else {
		return <td>{text}</td>
	}
}
export const bestMetaLink = (addon) => addon.updateURL ?? addon.downloadURL;
export const bestHomepageLink = (addon) => addon.website ?? addon.homepage ?? addon.homepageURL ?? addon.updateURL ?? addon.downloadURL;

const AddonRow = ({ id, setPage, search }) => {
	const { addon, setAddon } = useAddonItem(id);
	const { setListIds } = useAddonList();

	const [hasUpdate, setHasUpdate] = useState(null);

	const toggleAddon = () => {
		// setAddon does the call to the background and updates the list
		setAddon({ ...addon, enabled: !addon?.enabled })
	}

	const doRemoveAddon = async () => {
		const list = await removeAddon(id);
		// We need to update the list after the addon has been removed
		setListIds(list);
	}

	let actionButton = null;
	if (addon.source.toLowerCase() === 'local') {
		actionButton = (
			<Button
				onClick={() => setPage(`edit#${id}`)}
			>
				Edit
			</Button>
		);
	} else {
		const metaURL = bestMetaLink(addon);
		if (hasUpdate) {
			actionButton = <Button onClick={async () => {
				const list = await installUserscript(addon);
				if (!list.failed) {
					setListIds(list);
				}
				setHasUpdate(null);
			}}>Update</Button>
		} else if (metaURL && hasUpdate === null) {
			actionButton = <Button onClick={async () => {
				console.log(addon);

				const meta = await fetchUserscript(metaURL);

				if (semver.gt(semver.coerce(meta.version), semver.coerce(addon.version))) {
					setHasUpdate(true);
				} else {
					setHasUpdate(false);
				}
			}}>Check</Button>
		}
	}

	if (search.length > 0 && addon.name.toLowerCase().indexOf(search) < 0) {
		return null;
	}

	return (
		<tr>
			<td className="addon-list__actions--left">
				<Checkbox checked={addon.enabled} onChange={toggleAddon} />
				{actionButton}
			</td>

			<td>{addon.name ?? '?'}</td>
			<td>{addon.version ?? '?'}</td>
			<td>{addon.lastUpdated ? new Date(addon.lastUpdated).toString() : '?'}</td>
			<MaybeSourceLink addon={addon} />
			<MaybeHomepageLink addon={addon} />

			<td className="addon-list__actions">
				{/* <a href="#">Home</a> */}
				<Button negative onClick={doRemoveAddon}>X</Button>
			</td>
		</tr>
	);
}

const AddonList = ({ setPage, search }) => {
	const { list, toggleAll, listIds } = useAddonList();

	// Calculate toggle all checkbox state when list changes
	const [toggleAllState, setToggleAllState] = useState(false);
	useEffect(() => {
		setToggleAllState(!list.some(a => !a.enabled));
	}, [list]);

	if (!list || !list.length) {
		return (
			<div className="addon-list-empty">
				No addons installed yet!
			</div>
		);
	}

	return (
		<table className="addon-list">
			<thead>
				<tr>
					<th>
						<Checkbox checked={toggleAllState} onChange={() => {
							// Toggle all
							toggleAll(!toggleAllState);

							setToggleAllState(!toggleAllState);
						}} />
						Actions
					</th>
					<th>Addon</th>
					<th>Version</th>
					<th>Last Updated</th>
					<th>Source</th>
					<th>Author</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{
					listIds
						.map(a => <AddonRow key={a} id={a} setPage={setPage} search={search} />)
				}
			</tbody>
		</table>
	);
}

export { AddonList }
