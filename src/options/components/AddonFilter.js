import React from 'react';
import { Button, Input } from '@kckckc/isleward-util';

import { useAddonList } from '../useAddons';

import './AddonFilter.scss';

const maybePlural = (n) => n != 1 ? 's' : '';

const AddonFilter = ({ setPage, search, setSearch }) => {
	const { list } = useAddonList();

	const enabledCount = list.filter(a => a.enabled).length;
	const totalCount = list.length;
	const errorCount = 0;
	const updateCount = 0;

	return (
		<div className="addon-filter">
			<div className="addon-filter__row">
				<div className="addon-filter__info">
					<span className="addon-filter__info--bright">{enabledCount}</span>
					/
					<span className="addon-filter__info--bright">{totalCount}</span>
					{` addon${maybePlural(totalCount)} enabled`}
					{
						updateCount > 0 && (<>
							{" "}-{" "}
							<span className="addon-filter__info--updates">
								{updateCount} update{maybePlural(updateCount)} available
							</span>
						</>)
					}
					{
						errorCount > 0 && (<>
							{" "}-{" "}
							<span className="addon-filter__info--error">
								{errorCount} error{maybePlural(errorCount)}
							</span>
						</>)
					}
				</div>
			</div>

			<div className="addon-filter__row">
				<Input
					value={search}
					setValue={setSearch}
					placeholder="Search installed addons..."
				/>

				<div className="addon-filter__action-buttons">
					<Button onClick={() => setPage('edit')}>New Addon</Button>
					{/* <Button>Check All</Button>
					<Button>Update All</Button> */}
				</div>
			</div>
		</div>
	);
}

export { AddonFilter }
