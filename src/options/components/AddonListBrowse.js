import React, { useState, useEffect } from 'react';
import { Button } from '@kckckc/isleward-util';

import { fetchUserscript, installUserscript } from '../requests';
import { useRepos } from '../useRepos';
import { useAddonList } from '../useAddons';

import './AddonList.scss';
import { MaybeSourceLink, bestMetaLink, bestHomepageLink } from './AddonList';

const AddonRow = ({ data, search }) => {
	const [addon, setAddon] = useState({});

	const { setListIds } = useAddonList();

	// Fetch addon data
	useEffect(() => {
		(async () => {
			let newAddon = {
				...data,
			}

			// If we have an updateURL or downloadURL
			let metaSource = bestMetaLink(newAddon);
			if (metaSource) {
				// Request the background script to fetch and parse the userscript header
				const userscriptHeaders = await fetchUserscript(metaSource);

				// Overwrite userscript headers with properties from repository
				newAddon = {
					...userscriptHeaders,
					...newAddon,
				}
			}

			// Guess an ID if missing
			if (!newAddon.id) {
				if (newAddon.namespace) {
					newAddon.id = `${newAddon.namespace}:${newAddon.name}`
				} else {
					newAddon.id = newAddon.name;
				}
			}

			// Homepage link
			newAddon.linkTo = bestHomepageLink(newAddon);

			setAddon(newAddon);
		})();
	}, [data])

	let actionButton = (
		<Button
			onClick={async () => {
				if (!addon.downloadURL) {
					console.log('Missing downloadURL for addon:');
					console.log(addon);
					return;
				}

				const list = await installUserscript(addon);
				if (!list.failed) {
					setListIds(list);
				}
			}}
		>
			Install
		</Button>
	);

	if (search.length > 0 && addon.name.toLowerCase().indexOf(search) < 0) {
		return null;
	}

	return (
		<tr>
			<td className="addon-list__actions--left">
				{actionButton}
			</td>

			<td>{addon.name ?? '?'}</td>
			<td>{addon.version ?? '?'}</td>
			<td />
			<MaybeSourceLink addon={addon} />
			<td>{addon.author ?? '?'}</td>

			<td className="addon-list__actions">
				{addon.linkTo && <a href={addon.linkTo}>Link</a>}
			</td>
		</tr>
	);
}

const AddonListBrowse = ({ search }) => {
	const { fetchedLists } = useRepos();

	const flatAddons = fetchedLists.map(
		r => (r.list ?? [])
			.map(a => ({
				...a,
				source: r.name,
				sourceURL: r.url,
			}))
	).flat();

	if (!flatAddons.length) {
		return (
			<div className="addon-list-empty">
				No addons found!
			</div>
		);
	}

	return (
		<table className="addon-list">
			<thead>
				<tr>
					<th>
						Actions
					</th>
					<th>Addon</th>
					<th>Version</th>
					<th></th>
					<th>Source</th>
					<th>Author</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{flatAddons.map((a, i) => <AddonRow key={i} data={a} search={search} />)}
			</tbody>
		</table>
	);
}

export { AddonListBrowse }