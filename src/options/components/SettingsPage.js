import React, { useState } from 'react';
import { Button, Input } from '@kckckc/isleward-util';

import { useRepos } from '../useRepos';
import { useLocalList } from '../useLocal';
import { clearStorage } from '../requests';

import './SettingsPage.scss';

const SettingsPage = () => {
	const { setList, fetchedLists, resetRepos } = useRepos();
	const { localList, setLocalList } = useLocalList();

	const [editUrl, setEditUrl] = useState('');
	const addRepo = () => {
		if (editUrl === '') return;

		setList(prev => {
			if (prev.includes(editUrl)) return prev;

			return [...prev, editUrl];
		});
		setEditUrl('');
	}

	const removeRepo = (url) => {
		setList(prev => {
			return prev
				.filter(r => r !== url);
		});
	}

	const [editLocalFile, setEditLocalFile] = useState('');
	const addLocalFile = () => {
		if (editLocalFile === '') return;

		setLocalList(prev => {
			if (prev.includes(editLocalFile)) return prev;

			return [...prev, editLocalFile];
		});
		setEditLocalFile('');
	}

	const removeLocalFile = (url) => {
		setLocalList(prev => {
			return prev
				.filter(r => r !== url);
		});
	}

	return (
		<div className="settings-page">
			<div className="settings-page__row">
				<div className="settings-page__header">About</div>
			</div>

			<div className="settings-page__row">
				<div>See <a href="https://gitlab.com/Isleward-Modding/addons/addonmanager/-/tree/master#isleward-addon-manager">here</a>.</div>
			</div>

			<div className="settings-page__row">
				<div className="settings-page__header">Addon Repositories</div>
			</div>

			<div className="settings-page__row">
				<div>
					You can customize which JSON files are used to create the browse list by
					adding or removing URLs here.
				</div>
			</div>

			<div className="settings-page__row">
				<Input
					value={editUrl}
					setValue={setEditUrl}
					placeholder="Repository URL..."
				/>

				<Button
					onClick={addRepo}
				>
					Add Repository
				</Button>
				<Button
					onClick={resetRepos}
				>
					Reset to Default
				</Button>
			</div>

			<div className="settings-page__row">
				<React.Suspense fallback="Loading...">
					{
						fetchedLists.length ? (
							<ul>
								{
									fetchedLists.map(list => (
										<li key={list.url}>
											{list.name ? list.name : (
												<span className="settings-page__error">Failed to load!</span>
											)} - <a href={list.url}>{list.url}</a> (<a
												className="settings-page__remove"
												href="#"
												onClick={() => removeRepo(list.url)}
											>remove</a>)
										</li>
									))
								}
							</ul>
						) : (
							<div className="settings-page__empty-repos">No repositories yet!</div>
						)
					}
				</React.Suspense>
			</div>

			<div className="settings-page__row">
				<div className="settings-page__header">Load From URL</div>
			</div>

			<div className="settings-page__row">
				<div className="settings-page__error">
					You probably shouldn&apos;t use this unless you know what you&apos;re doing!!
					If you&apos;re not using this for development, you shouldn&apos;t use it!
				</div>
			</div>

			<div className="settings-page__row">
				<div>
					You can add URLs here pointing to userscript files.
					Every page refresh or hot reload (Ctrl+M) will re-fetch the file from the URL.
					Filesystem (file://) URLs are supported.
					For example, this could be the single JS file that you&apos;re editing, or
					the output of a bundler like Webpack.
					You can also point to a localhost URL where the file you&apos;re editing is hosted.
					It may be necessary to grant the extension access to file URLs.
				</div>
			</div>

			<div className="settings-page__row">
				<Input
					value={editLocalFile}
					setValue={setEditLocalFile}
					placeholder="file:/// URL..."
				/>

				<Button
					onClick={addLocalFile}
				>
					Add From URL
				</Button>
			</div>

			<div className="settings-page__row">
				<React.Suspense fallback="Loading...">
					{
						localList.length ? (
							<ul>
								{
									localList.map(url => (
										<li key={url}>
											{url} (<a
												className="settings-page__remove"
												href="#"
												onClick={() => removeLocalFile(url)}
											>remove</a>)
										</li>
									))
								}
							</ul>
						) : (
							<div className="settings-page__empty-repos">No filesystem files yet.</div>
						)
					}
				</React.Suspense>
			</div>

			<div className="settings-page__row">
				<div className="settings-page__header">Misc</div>
			</div>

			<div className="settings-page__row">
				<div>
					&quot;Clear Storage&quot; will remove all addons and repositories.
				</div>
			</div>

			<div className="settings-page__row">
				<div>
					<Button onClick={clearStorage} negative>Clear Storage</Button>
				</div>
			</div>
		</div>
	);
}

export { SettingsPage }
