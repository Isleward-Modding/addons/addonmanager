import React, { useState } from 'react';
import { Button, Input } from '@kckckc/isleward-util';
import hat from 'hat';
import semverValid from 'semver/functions/valid';

// CodeMirror
import { Controlled as CodeMirror } from 'react-codemirror2';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/dracula.css';

import { useAddonItem, useAddonList } from '../useAddons';

import './EditPage.scss';

const defaultText = `// AddonManager starter template. More information here: https://gitlab.com/Isleward-Modding/addons/addonmanager#addon-development
// Please leave these next 2 lines to properly load/reload your addon!
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'example' : ADDON_ID,

	name: 'Example',

	init: function () {
		// window.addons.events is guaranteed to be available now
		console.log('Hello World!');
	},

	cleanup: function () {
		// Do stuff, if using AddonManager
	}
});
`;

const EditPage = ({ openId, setPage }) => {
	const { listIds, updateList } = useAddonList();

	const [editId, setEditId] = useState(() => `addon-${hat()}`);
	const [editVersion, setEditVersion] = useState('1.0.0');
	const [editName, setEditName] = useState('New Addon');
	const [editAuthor, setEditAuthor] = useState('');
	const [editText, setEditText] = useState(defaultText);

	const { addon: loadFrom } = useAddonItem(openId);
	const [initialLoad, setInitialLoad] = useState(false);
	if (!initialLoad && openId !== '' && loadFrom) {
		setEditId(loadFrom.id);
		setEditVersion(loadFrom.version);
		setEditName(loadFrom.name);
		setEditAuthor(loadFrom.author);
		setEditText(loadFrom.raw);

		setInitialLoad(true);
	}

	const missingFieldError =
		editId === '' || editVersion === '' || editName === '';
	const idError =
		editId === '' || (openId === '' && listIds.includes(editId));
	const versionError = editVersion === '' || !semverValid(editVersion);

	const createAddon = async () => {
		if (missingFieldError) {
			alert('Please fill in the ID, version, and name fields!');
			return;
		}

		if (idError) {
			alert(
				'This ID is already in use! Choose a different one (IDs only matter if you will be sharing/reusing this addon).'
			);
			return;
		}

		if (versionError) {
			alert('Invalid version! Please follow SemVer (ex: 1.2.3).');
			return;
		}

		const addon = {
			id: editId,
			name: editName,
			version: editVersion,
			lastUpdated: Date.now(),
			source: 'Local',
			author: editAuthor,
			raw: editText,
			enabled: true,
		};

		updateList([addon]);
		setPage('home');
	};

	return (
		<div className="edit-page">
			<div className="edit-page__info">
				<div className="edit-page__row">
					<Input
						value={editId}
						setValue={setEditId}
						placeholder="ID (required)"
						indicator={idError ? '(!)' : ''}
						indicatorColor="red"
					/>
					<Input
						value={editVersion}
						setValue={setEditVersion}
						placeholder="Version (required)"
						indicator={versionError ? '(!)' : ''}
						indicatorColor="red"
					/>
					<Button onClick={createAddon}>Save</Button>
					<Button negative onClick={() => setPage('home')}>
						Cancel
					</Button>
				</div>
				<div className="edit-page__row">
					<Input
						value={editName}
						setValue={setEditName}
						placeholder="Addon Name (required)"
						indicator={editName === '' ? '(!)' : ''}
						indicatorColor="red"
					/>
					<Input
						value={editAuthor}
						setValue={setEditAuthor}
						placeholder="Author (optional)"
					/>
				</div>
			</div>

			<CodeMirror
				value={editText}
				options={{
					mode: 'javascript',
					theme: 'dracula',
					lineNumbers: true,
				}}
				onBeforeChange={(editor, data, value) => {
					setEditText(value);
				}}
				// onChange={(editor, data, value) => {
				//
				// }}
			/>
		</div>
	);
};

export { EditPage };
