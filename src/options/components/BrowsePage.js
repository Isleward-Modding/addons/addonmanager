import React, { useState } from 'react';

import { BrowseFilter } from './BrowseFilter';
import { AddonListBrowse } from './AddonListBrowse';

import './BrowsePage.scss';

const BrowsePage = ({ setPage }) => {
	const [search, setSearch] = useState('');

	return (
		<div className="browse-page">
			<BrowseFilter setPage={setPage} search={search} setSearch={setSearch} />
			<AddonListBrowse setPage={setPage} search={search} />
		</div>
	);
}

export { BrowsePage }
