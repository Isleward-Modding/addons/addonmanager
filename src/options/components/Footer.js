import React from 'react';

const Footer = () => (
	<footer className="footer">
		<div className="footer__text">
			Addon Manager <span className="footer__highlight">v{__APPVERSION__}</span>
		</div>
	</footer>
);

export { Footer }
