import {
	atom,
	useRecoilValue,
	useRecoilCallback,
} from 'recoil';

import { fetchLocalList, setLocalList } from './requests';

const syncListEffect = ({ setSelf, onSet, trigger }) => {
	const loadFromBackground = async () => {
		setSelf(await fetchLocalList());
	}

	if (trigger === 'get') {
		loadFromBackground();
	}

	onSet((value, _, isReset) => {
		isReset
			? loadFromBackground()
			: setLocalList(value)
	});
}

const localListState = atom({
	key: 'LocalListState',
	default: [],
	effects_UNSTABLE: [
		syncListEffect
	]
});

export const useLocalList = () => {
	const localList = useRecoilValue(localListState) ?? [];

	const setLocalList = useRecoilCallback(
		({ set }) => (list) => {
			set(localListState, list);
		},
		[],
	);

	return {
		localList,
		setLocalList,
	}
}
