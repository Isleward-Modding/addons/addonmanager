import {
	atom,
	atomFamily,
	selector,
	selectorFamily,
	useRecoilValue,
	useRecoilCallback,
} from 'recoil';

import { fetchRepoList, setRepoList, resetRepoList, fetchRepo } from './requests';

const syncListEffect = ({ setSelf, onSet, trigger }) => {
	const loadFromBackground = async () => {
		setSelf(await fetchRepoList());
	}

	if (trigger === 'get') {
		loadFromBackground();
	}

	onSet((value, _, isReset) => {
		isReset
			? loadFromBackground() 
			: setRepoList(value)
	});
}

const repoListState = atom({
	key: 'RepoListState',
	default: [],
	effects_UNSTABLE: [
		syncListEffect
	]
});

const repoState = atomFamily({
	key: 'RepoState',
	default: () => null
});

const repoQuery = selectorFamily({
	key: 'RepoQuery',
	get: (id) => async ({ get }) => {
		// Check cached value
		const s = get(repoState(id));

		// If we have it already, return it
		if (s !== null) {
			return s;
		}

		// Otherwise fetch it from the background worker
		let data = await fetchRepo(id);
		data.url = id;
		return data;
	}
});

const queryAllSelector = selector({
	key: 'QueryAllSelector',
	get: ({ get }) => {
		const ids = get(repoListState);
		return ids.map(i => get(repoQuery(i)));
	}
})

export const useRepos = () => {
	const list = useRecoilValue(repoListState);

	const fetchedLists = useRecoilValue(queryAllSelector);

	const setList = useRecoilCallback(
		({ set }) => (list) => {
			set(repoListState, list);
		},
		[],
	);

	const resetRepos = useRecoilCallback(
		({ reset }) => async () => {
			await resetRepoList();
			reset(repoListState);
		}
	)

	return {
		list,
		setList,
		fetchedLists,
		resetRepos,
	}
}
