// https://betterprogramming.pub/implementing-list-items-with-react-and-recoil-c58f75e307dc

import {
	atom,
	atomFamily,
	selector,
	selectorFamily,
	useRecoilValue,
	useRecoilCallback,
} from 'recoil';

import { getAddon, setAddon } from './requests';

const addonListState = atom({
	key: 'AddonListState',
	default: [],
});

const addonState = atomFamily({
	key: 'AddonState',
	default: () => null
});

const addonQuery = selectorFamily({
	key: 'AddonQuery',
	get: (id) => async ({ get }) => {
		// Check cached value
		const s = get(addonState(id));

		// If we have it already, return it
		if (s !== null) {
			return s;
		}

		// Otherwise fetch it from the background worker
		return await getAddon(id);
	},
	set: (id) => ({ get, set }, newValue) => {
		// Update in storage
		setAddon(newValue);

		// Update locally
		set(addonState(id), newValue);

		// Check if already in list
		if (get(addonListState).find(i => i === newValue.id)) return;

		// Add new id to list
		set(addonListState, (prev) => [...prev, newValue.id]);
	},
});

const mapListSelector = selector({
	key: 'MapListSelector',
	get: ({ get }) => {
		const ids = get(addonListState);
		return ids.map(i => get(addonQuery(i)));
	}
})

export const useAddonList = () => {
	const list = useRecoilValue(mapListSelector);

	const listIds = useRecoilValue(addonListState);

	const updateList = useRecoilCallback(
		({ set }) => (list) => {
			list.forEach(l => {
				set(addonQuery(l.id), l);
			});
		},
		[],
	);

	const setListIds = useRecoilCallback(
		({ set }) => (list) => {
			set(addonListState, list);
		},
		[],
	);

	const toggleAll = useRecoilCallback(
		({ set }) => (val) => {
			list.forEach(l => {
				set(addonQuery(l.id), (prev) => ({ ...prev, enabled: val }));
			});
		}
	);

	return {
		list,
		updateList,
		listIds,
		setListIds,
		toggleAll,
	}
}

export const useAddonItem = (id) => {
	const addon = useRecoilValue(addonQuery(id));

	const setAddon = useRecoilCallback(
		({ set }) => (data) => {
			set(addonQuery(data.id), data);
		},
		[],
	);

	return {
		addon,
		setAddon,
	}
}
