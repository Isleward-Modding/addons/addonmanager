// Addons

const fetchList = () => {
	return new Promise((res) => {
		chrome.runtime.sendMessage({ request: 'fetchList' }, (data) => {
			res(data);
		});
	});
}
const setAddon = (data) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'setAddon', data }, (response) => {
			resolve(response);
		});
	});
}
const getAddon = (id) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'getAddon', id }, (response) => {
			resolve(response);
		});
	});
}
const removeAddon = (id) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'removeAddon', id }, (response) => {
			resolve(response);
		});
	})
}

// Local addons

const fetchLocalList = () => {
	return new Promise((res) => {
		chrome.runtime.sendMessage({ request: 'fetchLocalList' }, (data) => {
			res(data);
		});
	});
}
const setLocalList = (data) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'setLocalList', data }, (response) => {
			resolve(response);
		});
	});
}

// Repos

const fetchRepoList = () => {
	return new Promise((res) => {
		chrome.runtime.sendMessage({ request: 'fetchRepoList' }, (data) => {
			res(data);
		});
	});
}
const setRepoList = (data) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'setRepoList', data }, (response) => {
			resolve(response);
		});
	});
}
const resetRepoList = () => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'resetRepoList' }, (response) => {
			resolve(response);
		});
	});
}
const fetchRepo = (data) => {
	return new Promise((res) => {
		chrome.runtime.sendMessage({ request: 'fetchRepo', data }, (data) => {
			res(data);
		});
	});
}

// Userscripts

const fetchUserscript = (data) => {
	return new Promise((res) => {
		chrome.runtime.sendMessage({ request: 'fetchUserscript', data }, (data) => {
			res(data);
		});
	});
}
const installUserscript = (data) => {
	return new Promise((resolve) => {
		chrome.runtime.sendMessage({ request: 'installUserscript', data }, (result) => {
			if (result.failed) {
				alert('Failed to install userscript');
				console.error('Failed to install userscript for', data);
			}

			resolve(result);
		});
	});
}

// Clear storage and reload
// TODO promisify
const clearStorage = () => {
	chrome.runtime.sendMessage({ request: 'clearStorage' }, () => {
		alert('Cleared storage!');
		window.location.reload();
	});
}

export {
	fetchList,
	setAddon,
	getAddon,
	removeAddon,
	fetchRepoList,
	setRepoList,
	resetRepoList,
	fetchRepo,
	fetchLocalList,
	setLocalList,
	fetchUserscript,
	installUserscript,
	clearStorage,
}
