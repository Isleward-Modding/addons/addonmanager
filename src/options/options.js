import React, { Suspense, useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { RecoilRoot } from 'recoil';

import { Navigation } from './components/Navigation';
import { Footer } from './components/Footer';
import { EditPage } from './components/EditPage';
import { BrowsePage } from './components/BrowsePage';
import { SettingsPage } from './components/SettingsPage';
import { AddonList } from './components/AddonList';
import { AddonFilter } from './components/AddonFilter';

import { fetchList } from './requests';
import { useAddonList } from './useAddons';

import './options.scss';
import '../styles/main.scss';

/// Pages

const HomePage = ({ setPage }) => {
	const [search, setSearch] = useState('');

	return (
		<div className="home-page">
			<AddonFilter setPage={setPage} search={search} setSearch={setSearch} />
			<AddonList setPage={setPage} search={search} />
		</div>
	);
}

/// App

const App = () => {
	// Routing
	const [page, setPage] = useState('home');
	const pageType = page.split('#')[0];
	const pageId = page.split('#').slice(1).join('#'); // ?

	// Fetch once on first load
	const { setListIds } = useAddonList();
	useEffect(() => {
		(async () => {
			const data = await fetchList();
			setListIds(data.filter(d => d !== null));
		})();
	}, []);

	return (
		<div className="app">
			<Navigation page={pageType} setPage={setPage} />

			<Suspense fallback="">
				{pageType === 'home' && <HomePage setPage={setPage} />}
				{pageType === 'edit' && <EditPage openId={pageId} setPage={setPage} />}
				{pageType === 'browse' && <BrowsePage setPage={setPage} />}
				{pageType === 'settings' && <SettingsPage setPage={setPage} />}
			</Suspense>

			{!['edit'].includes(pageType) &&
				<div className="app__fill" />
			}

			<Footer />
		</div>
	);
}

const WrappedApp = () => (
	// It's probably bad to wrap everything in a suspense like this?
	<Suspense fallback="">
		<RecoilRoot>
			<App />
		</RecoilRoot>
	</Suspense>
);

ReactDOM.render(<WrappedApp />, document.getElementById("root"));
