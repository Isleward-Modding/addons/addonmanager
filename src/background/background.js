import { parseUserscript } from './parseUserscript';

// Open options on icon click
chrome.action.onClicked.addListener((/*tab*/) => {
	chrome.runtime.openOptionsPage();

	// chrome.scripting.executeScript({
	// 	target: { tabId: tab.id },
	// 	files: ['content.js']
	// });

	// Test
	// chrome.action.setBadgeText({ tabId: tab.id, text: "100" });
	// chrome.action.setBadgeBackgroundColor({ tabId: tab.id, color: "#f00" });
});

// Open options on extension install
chrome.runtime.onInstalled.addListener((o) => {
	if (o.reason === 'install') {
		chrome.runtime.openOptionsPage();
	}
});

const DEFAULT_REPOS = [
	'https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/repositories/main.json'
];

// Storage API
// Should we use the synced version?
const store = chrome.storage.local;

// Helpers
const installAddon = (data) => {
	return new Promise((resolve) => {
		store.get(['list'], ({ list }) => {
			if (!list) {
				list = [];
			}

			if (!list.includes(data.id)) {
				list.push(data.id);
			}

			store.set({ list, [`addon-${data.id}`]: data }, () => {
				resolve(list);
			});
		});
	});
}

// Listen for requests from options page and injected script
chrome.runtime.onMessage.addListener((msg, sender, resolve) => {
	console.log(msg);

	if (msg?.request === 'clearStorage') {
		store.clear(() => { resolve() });
		return true;
	} else if (msg?.request === 'fetchList') {
		store.get(['list'], ({ list }) => {
			if (!list) {
				list = [];
			}

			list = list.filter(l => l !== null);

			resolve(list);
		});
		return true;
	} else if (msg?.request === 'setAddon') {
		installAddon(msg.data).then((list) => resolve(list));
		return true;
	} else if (msg?.request === 'getAddon') {
		const key = `addon-${msg.id}`;
		store.get([key], (res) => {
			let data = res[key];

			resolve(data);
		});
		return true;
	} else if (msg?.request === 'removeAddon') {
		store.get(['list'], ({ list }) => {
			if (!list) {
				list = [];
			}

			const newList = list.filter(a => a !== msg.id);

			store.set({ list: newList, [`addon-${msg.id}`]: null }, () => {
				resolve(newList);
			});
		});
		return true;
	} else if (msg?.request === 'getEnabledAddons') {
		store.get(['list', 'localList'], ({ list, localList }) => {
			if (!list) {
				list = [];
			}
			if (!localList) {
				localList = [];
			}

			store.get(list.map(a => `addon-${a}`), (res) => {
				const enabled = Object.values(res).filter(a => a.enabled);

				resolve({ enabled, localList });
			});
		});
		return true;
	} else if (msg?.request === 'fetchLocalList') {
		store.get(['localList'], ({ localList }) => {
			resolve(localList ?? []);
		});
		return true;
	} else if (msg?.request === 'setLocalList') {
		store.set({ localList: msg.data }, () => {
			resolve();
		});
		return true;
	} else if (msg?.request === 'fetchRepoList') {
		store.get(['repolist'], ({ repolist }) => {
			resolve(repolist ?? []);
		});
		return true;
	} else if (msg?.request === 'setRepoList') {
		store.set({ repolist: msg.data }, () => {
			resolve();
		});
		return true;
	} else if (msg?.request === 'resetRepoList') {
		store.set({ repolist: DEFAULT_REPOS }, () => {
			resolve();
		});
		return true;
	} else if (msg?.request === 'fetchRepo') {
		fetch(msg.data)
			.then(res => res.json())
			.then(res => resolve(res))
			.catch(() => resolve({}));
		return true;
	} else if (msg?.request === 'fetchUserscript') {
		fetch(msg.data)
			.then(res => res.text())
			.then(res => resolve(parseUserscript(res)))
			.catch(() => resolve({}));
		return true;
	} else if (msg?.request === 'installUserscript') {
		fetch(msg.data.downloadURL)
			.then(res => res.text())
			.then(text => {
				// The host probably 404ed?
				if (text.toLowerCase().startsWith('<!doctype') || text.toLowerCase().startsWith('<html')) {
					resolve({ failed: true });
					return;
				}

				const meta = parseUserscript(text);

				if (!meta.id) {
					if (meta.namespace) {
						meta.id = `${meta.namespace}:${meta.name}`
					} else {
						meta.id = meta.name;
					}
				}

				const addon = {
					// Might be provided by repos
					downloadURL: msg.data.downloadURL,
					updateURL: msg.data.updateURL,

					// Copy userscript headers
					...meta,

					// Add manager metadata
					lastUpdated: Date.now(),
					source: msg.data.source,
					sourceURL: msg.data.sourceURL,
					raw: text,
					enabled: true,
				}

				installAddon(addon).then((data) => resolve(data));
			})
			.catch(() => resolve({ failed: true }))
		return true;
	}
});
