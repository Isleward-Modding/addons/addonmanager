// https://wiki.greasespot.net/Metadata_Block
// https://www.tampermonkey.net/documentation.php
// https://github.com/violentmonkey/violentmonkey/blob/54f78130379a00afa4419bad0723a8b1363b0cf6/src/common/consts.js#L24
// https://github.com/violentmonkey/violentmonkey/blob/f3872f77fba99b4dee867a13e5198074eb5ca3c0/src/background/utils/script.js#L64

const METABLOCK_RE = /(?:^|\n)\s*\/\/\x20==UserScript==([\s\S]*?\n)\s*\/\/\x20==\/UserScript==|$/;

// We don't handle arrays since we just want the name, namespace, author, version, and URLs.
const parseUserscript = (code) => {
	const meta = {};

	const metaBody = code.match(METABLOCK_RE)[1] || '';

	metaBody.replace(/(?:^|\n)\s*\/\/\x20(@\S+)(.*)/g, (_match, rawKey, rawValue) => {
		const [keyName, locale] = rawKey.slice(1).split(':');
		const camelKey = keyName.replace(/[-_](\w)/g, (m, g) => g.toUpperCase());
		const key = locale ? `${camelKey}:${locale.toLowerCase()}` : camelKey;
		const val = rawValue.trim();
		meta[key] = val;
	});

	return meta;
}

export { parseUserscript }
