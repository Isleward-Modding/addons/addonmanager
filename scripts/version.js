const package = require('../package.json');

function getVersion() {
	return package.version;
}

module.exports = { getVersion }
