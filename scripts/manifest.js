const { getVersion } = require('./version.js');

function readManifest() {
	return require('../src/manifest.json');
}

function buildManifest() {
	const data = readManifest();

	data.version = getVersion();

	return data;
}

module.exports = { buildManifest }
