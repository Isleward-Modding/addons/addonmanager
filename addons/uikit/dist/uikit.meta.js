// ==UserScript==
// @name        UI Kit
// @version     1.0.0
// @author      kckckc
// @description UI Kit addon for Isleward.
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// @grant       none
// @downloadURL https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/uikit/dist/uikit.user.js
// @updateURL   https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/uikit/dist/uikit.meta.js
// ==/UserScript==
