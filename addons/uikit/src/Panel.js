import React, {
	useCallback,
	useEffect,
	useLayoutEffect,
	useState,
	useRef,
} from 'react';
import classnames from 'classnames';
import styles from './uikit.module.css';
import throttle from 'lodash.throttle';

const SNAP_CONFIG_DEFAULT = [
	/*
	{
		selector: '.className',
		leftInner: true,
		leftOuter: true,
		rightInner: true,
		rightOuter: true,
		topInner: true,
		topOuter: true,
		bottomInner: true,
		bottomOuter: true,
	},
	*/
	{
		// For window edges
		selector: '.ui-container',
		leftInner: true,
		rightInner: true,
		topInner: true,
		bottomInner: true,
	},
	{
		selector: '.uiLogin > .right',
		leftInner: true,
		leftOuter: true,
		rightInner: true,
		rightOuter: true,
		topInner: true,
		topOuter: true,
		bottomInner: true,
		bottomOuter: true,
	},
	{
		selector: '.hudBox',
		leftInner: true,
		leftOuter: true,
		rightInner: true,
		rightOuter: true,
		topInner: true,
		topOuter: true,
		bottomInner: true,
		bottomOuter: true,
	},
	{
		selector: '.uiMenu',
		leftInner: true,
		leftOuter: true,
		rightInner: true,
		rightOuter: true,
		topInner: true,
		topOuter: true,
		bottomInner: true,
		bottomOuter: true,
	},
];

function getGuidelines(snapTo) {
	const list = [];

	const uiContainer = document.documentElement;

	snapTo.forEach((target) => {
		// We could do querySelectorAll, but when restoring snaps we don't know which of multiple possible els we snapped to previously
		const el = uiContainer.querySelector(target.selector);

		if (!el) return;

		const rect = el.getBoundingClientRect();

		list.push({ rect, ...target });
	});

	return list;
}

export function Panel({
	visible = true,
	children,
	className,
	saveAs,
	enableSnap = true,
	snapTo = null,
	snapThreshold = 20,
	snapThresholdCross = 15,
}) {
	const ref = useRef(null);

	const [position, setPosition] = useState({
		x: 0,
		y: 0,
	});

	const [snappedPosition, setSnappedPosition] = useState({
		x: 0,
		y: 0,
	});

	const [relativePosition, setRelativePosition] = useState({
		x: 0,
		y: 0,
	});

	const [dragging, setDragging] = useState(false);

	const [restoreGuideline, setRestoreGuideline] = useState(null);

	const [manualMoved, setManualMoved] = useState(false);
	const [tryRestore, setTryRestore] = useState(0);

	const onMouseDown = useCallback((e) => {
		if (e.button !== 0) return;

		let pos = ref.current.getBoundingClientRect();
		setDragging(true);
		setRelativePosition({
			x: e.pageX - pos.left,
			y: e.pageY - pos.top,
		});
		setManualMoved(true);
	});

	// Drag effect
	useEffect(() => {
		if (!dragging) return;

		const onMouseMove = throttle((e) => {
			setPosition({
				x: e.pageX - relativePosition.x,
				y: e.pageY - relativePosition.y,
			});

			e.stopPropagation();
			e.preventDefault();
		}, 20);

		const onMouseUp = (e) => {
			setDragging(false);

			e.stopPropagation();
			e.preventDefault();
		};

		document.addEventListener('mousemove', onMouseMove);
		document.addEventListener('mouseup', onMouseUp);

		return () => {
			document.removeEventListener('mousemove', onMouseMove);
			document.removeEventListener('mouseup', onMouseUp);
		};
	}, [dragging, relativePosition]);

	// Resize handler (resnap if snapped)
	useEffect(() => {
		if (!enableSnap) return;

		const handleResize = () => {
			if (!restoreGuideline) return;

			const myRect = ref.current.getBoundingClientRect();

			const uiContainer = document.documentElement;

			const gl = restoreGuideline;
			if (!gl) return;
			const snapToRect = uiContainer
				.querySelector(gl.selector)
				.getBoundingClientRect();

			const newPos = {
				x:
					gl.offsetX * -myRect.width +
					gl.percentX * snapToRect.width +
					snapToRect.left,
				y:
					gl.offsetY * -myRect.height +
					gl.percentY * snapToRect.height +
					snapToRect.top,
			};
			setSnappedPosition(newPos);
		};

		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, [restoreGuideline, enableSnap]);

	// Clear guideline when snap is disabled
	useEffect(() => {
		if (!enableSnap) {
			setRestoreGuideline(null);
		}
	}, [enableSnap]);

	// Save guideline when it's changed
	useEffect(() => {
		if (!saveAs) return;

		if (!manualMoved) return;

		localStorage.setItem(
			`isleward_addon_panel_${saveAs}`,
			JSON.stringify(restoreGuideline)
		);
	}, [restoreGuideline, manualMoved, saveAs]);

	// Attempt to restore to stored
	useEffect(() => {
		if (!saveAs) return;
		if (manualMoved) return;

		const loaded = JSON.parse(
			localStorage.getItem(`isleward_addon_panel_${saveAs}`)
		);
		if (loaded) {
			const myRect = ref.current.getBoundingClientRect();

			const uiContainer = document.documentElement;

			const gl = loaded;
			const restoreToEl = uiContainer.querySelector(gl.selector);

			if (!restoreToEl) return;

			const snapToRect = restoreToEl.getBoundingClientRect();

			setSnappedPosition({
				x:
					gl.offsetX * -myRect.width +
					gl.percentX * snapToRect.width +
					snapToRect.left,
				y:
					gl.offsetY * -myRect.height +
					gl.percentY * snapToRect.height +
					snapToRect.top,
			});
			setManualMoved(true);
			setRestoreGuideline(loaded);
		}
	}, [manualMoved, saveAs, visible, tryRestore]);

	// Keep trying to restore until it's moved manually or restored (which also sets the manual move flag)
	useEffect(() => {
		if (!saveAs) return;
		if (manualMoved) return;

		const i = setInterval(() => {
			setTryRestore((i) => i + 1);
		}, 200);

		return () => {
			clearInterval(i);
		};
	}, [manualMoved, saveAs]);

	// Snap to guidelines
	useLayoutEffect(() => {
		if (!enableSnap) {
			setSnappedPosition(position);
			return;
		}

		const list = getGuidelines(snapTo ?? SNAP_CONFIG_DEFAULT);

		let snapped = null;

		const domRect = ref.current.getBoundingClientRect();
		const myRect = {
			left: position.x,
			top: position.y,
			right: position.x + domRect.width,
			bottom: position.y + domRect.height,

			width: domRect.width,
			height: domRect.height,
		};

		const oppositeSide = {
			left: 'right',
			right: 'left',
			top: 'bottom',
			bottom: 'top',
		};
		const axisMap = {
			left: 'X',
			right: 'X',
			top: 'Y',
			bottom: 'Y',
		};
		const highSideMap = {
			left: false,
			right: true,
			top: false,
			bottom: true,
		};

		for (let gl of list) {
			if (snapped) break; // Snap to first found

			const glRect = gl.rect;

			const insideVertical =
				myRect.bottom > glRect.top && myRect.top < glRect.bottom;
			const insideHorizontal =
				myRect.right > glRect.left && myRect.left < glRect.right;

			if (!insideVertical && !insideHorizontal) continue;

			const verticalAnchor = (myRect.top + myRect.bottom) / 2;
			const verticalPercent =
				(verticalAnchor - glRect.top) / (glRect.bottom - glRect.top);

			const horizontalAnchor = (myRect.left + myRect.right) / 2;
			const horizontalPercent =
				(horizontalAnchor - glRect.left) / (glRect.right - glRect.left);

			function checkSide(side, outer) {
				const mySide = outer ? oppositeSide[side] : side;

				if (Math.abs(myRect[mySide] - glRect[side]) < snapThreshold) {
					snapped = gl;

					const axis = axisMap[side];
					let crossAxis = axis === 'X' ? 'Y' : 'X';

					let crossAxisPercent =
						crossAxis === 'Y' ? verticalPercent : horizontalPercent;

					snapped[`percent${axis}`] = highSideMap[side] ? 1 : 0;
					snapped[`percent${crossAxis}`] = crossAxisPercent;
					snapped[`offset${axis}`] = highSideMap[side]
						? outer
							? 0
							: 1
						: outer
						? 1
						: 0;
					snapped[`offset${crossAxis}`] = 0.5;

					// Secondary snap to cross-axis edges

					const secondarySides =
						axis === 'X' ? ['top', 'bottom'] : ['left', 'right'];
					secondarySides.forEach((second, i) => {
						if (
							Math.abs(myRect[second] - glRect[second]) <
							snapThresholdCross
						) {
							snapped[`percent${crossAxis}`] = i;
							snapped[`offset${crossAxis}`] = i ? 1 : 0;
						}
					});

					return true;
				}

				return false;
			}

			if (insideVertical) {
				if (gl.leftInner && checkSide('left', false)) break;
				if (gl.leftOuter && checkSide('left', true)) break;
				if (gl.rightInner && checkSide('right', false)) break;
				if (gl.rightOuter && checkSide('right', true)) break;
			}

			if (insideHorizontal) {
				if (checkSide('top', false)) break;
				if (checkSide('top', true)) break;
				if (checkSide('bottom', false)) break;
				if (checkSide('bottom', true)) break;
			}
		}

		if (snapped) {
			setSnappedPosition({
				x:
					snapped.offsetX * -myRect.width +
					snapped.percentX * snapped.rect.width +
					snapped.rect.left,
				y:
					snapped.offsetY * -myRect.height +
					snapped.percentY * snapped.rect.height +
					snapped.rect.top,
			});
		} else {
			setSnappedPosition(position);
		}

		if (manualMoved) {
			setRestoreGuideline(snapped);
		}
	}, [position]);

	if (!visible) return null;

	return (
		<div
			ref={ref}
			className={classnames(styles.panel, className)}
			style={{
				left: `${snappedPosition.x}px`,
				top: `${snappedPosition.y}px`,
			}}
			onMouseDown={onMouseDown}
		>
			{children}
		</div>
	);
}
