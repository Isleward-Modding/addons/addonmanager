import React from 'react';
import { Panel } from './Panel.js';
import {
	createContainer,
	removeContainer,
	updateContainer,
} from './uiContainer.js';

// prettier-ignore
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'uikit' : ADDON_ID,
	name: 'uikit',

	// Exports
	Panel,
	e: React.createElement,

	// Internal
	_uis: [],

	// Lifecycle
	init: function () {
		console.log('uikit', 'Loaded');

		createContainer();
		updateContainer(this._uis);
	},

	cleanup: function () {
		removeContainer();
	},

	addElement: function (el) {
		this._uis.push(el);
		updateContainer(el);
	},
	removeElement: function (el) {
		this._uis = this._uis.filter((e) => e !== el);
		updateContainer(el);
	},
});
