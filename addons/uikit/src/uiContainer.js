import hat from 'hat';
import React from 'react';
import { render } from 'react-dom';
import styles from './uikit.module.css';

let container;
export function createContainer(uis) {
	container = document.createElement('div');
	container.id = `iwd-uikit-${hat().slice(0, 6)}`;
	container.className = styles.container;
	document.body.appendChild(container);
}

export function updateContainer(uis) {
	const app = <>{uis}</>;

	console.log(uis, container);

	render(app, container);
}

export function removeContainer() {
	render(null, container);

	document.body.removeChild(container);
}
