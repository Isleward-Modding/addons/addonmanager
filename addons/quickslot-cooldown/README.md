# Quickslot Cooldown

Adds a radial cooldown indicator to the quickslot HUD.

## Installation

One-click install using the Addon Manager's browse, or load the userscript like normal.
