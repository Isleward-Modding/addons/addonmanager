const updateInterval = 16;
const color1 = 'rgba(58,59,74,.9)';
const color2 = 'rgba(212,51,70,.75)';

function updateQuickslot() {
	const ui = $('.uiHud').data('ui');

	let percentString = '100%';
	if (
		typeof ui?.quickItem?.cd !== 'undefined' &&
		typeof ui?.quickItem?.cdMax !== 'undefined'
	) {
		let { cd, cdMax } = ui?.quickItem;

		if (cd !== 0) {
			let percent = (1 - cd / cdMax) * 100;
			percentString = `${percent}%`;
		}
	}

	$('.quickItem').css({
		background: `conic-gradient(${color1} ${percentString}, ${color2} 0)`,
		marginBottom: '0px',
	});
}

let timer = null;

// prettier-ignore
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'quickslot-cooldown' : ADDON_ID,
	name: 'Quickslot Cooldown',

	init: function() {
		timer = setInterval(updateQuickslot, updateInterval);
	},

	cleanup: function () {
		clearInterval(timer);
	}
});
