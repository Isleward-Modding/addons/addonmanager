// ==UserScript==
// @name        Quickslot Cooldown
// @version     1.1.1
// @author      kckckc
// @description Quickslot Cooldown addon for Isleward.
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// @grant       none
// @downloadURL https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/quickslot-cooldown/dist/quickslot-cooldown.user.js
// @updateURL   https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/quickslot-cooldown/dist/quickslot-cooldown.meta.js
// ==/UserScript==

(()=>{"use strict";function e(){const e=$(".uiHud").data("ui");let t="100%";if(void 0!==e?.quickItem?.cd&&void 0!==e?.quickItem?.cdMax){let{cd:i,cdMax:n}=e?.quickItem;if(0!==i){t=`${100*(1-i/n)}%`}}$(".quickItem").css({background:`conic-gradient(rgba(58,59,74,.9) ${t}, rgba(212,51,70,.75) 0)`,marginBottom:"0px"})}let t=null;(e=>{let t=i=>{let n=("undefined"==typeof unsafeWindow?globalThis:unsafeWindow)["undefined"==typeof ADDON_MANAGER?"addons":ADDON_MANAGER];n?n.register(e):setTimeout(t,200)};t()})({id:"undefined"==typeof ADDON_ID?"quickslot-cooldown":ADDON_ID,name:"Quickslot Cooldown",init:function(){t=setInterval(e,16)},cleanup:function(){clearInterval(t)}})})();