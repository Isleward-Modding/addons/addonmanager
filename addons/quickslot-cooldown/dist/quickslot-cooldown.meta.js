// ==UserScript==
// @name        Quickslot Cooldown
// @version     1.1.1
// @author      kckckc
// @description Quickslot Cooldown addon for Isleward.
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// @grant       none
// @downloadURL https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/quickslot-cooldown/dist/quickslot-cooldown.user.js
// @updateURL   https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/quickslot-cooldown/dist/quickslot-cooldown.meta.js
// ==/UserScript==
