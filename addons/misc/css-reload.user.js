// ==UserScript==
// @name        CSS Reload
// @version     1.0.0
// @author      kckckc
// @description CSS Reload addon for Isleward.
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// @grant       none
// ==/UserScript==

// Configure me
const CSS_RELOAD_KEYS = [','];

document.addEventListener('keypress', (e) => {
	if (!CSS_RELOAD_KEYS.includes(e.key)) return;

	// Change the query string so that a new version is retrieved from the server
	// https://stackoverflow.com/a/44010918/7455410
	for (let link of document.querySelectorAll('link[rel=stylesheet]')) {
		link.href = link.href.replace(/\?.*|$/, '?' + Date.now());
	}
});

console.log(
	`CSS Reload: press ${CSS_RELOAD_KEYS.map((k) => `"${k}"`).join(',')}`
);
