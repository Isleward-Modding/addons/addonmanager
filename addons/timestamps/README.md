# Timestamps

Customizable chat timestamps.

## Installation

One-click install using the Addon Manager's browse, or load the userscript like normal.

## Usage

`/timeformat <format>`: Set the timestamp format.
If the format is invalid it will be reset to the default.

`/timecolor <color>`: Set the timestamp color to a hex color or a preset color.
If `color` is blank, timestamps will be colored with the message color.
[Name <-> hex color list](https://gitlab.com/Isleward/isleward/-/blob/master/src/client/css/colors.less)

`/toggletimezone`: Toggle between formatting the local time and server time for timestamps

Examples:
* `/timeformat [hh:mm:ss a]` -> `[01:23:45 PM]`
* `/timeformat [HH:mm]` -> `[22:33]`
* `/timecolor grayD`

[Time format reference](https://www.unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table)

## TODO

* Create timestamp in a separate span
* Fixed-width timestamp

## Thanks

[Qndel's chat timestamps](https://gitlab.com/Isleward-Modding/addons/timestamp-in-chat/-/tree/master)
