import { format, utcToZonedTime } from 'date-fns-tz';

const formatVersion = 1;
const storageKeyFormat = `isleward_addon_timestamp_format_${formatVersion}`;
const storageKeyServerTime = `isleward_addon_timestamp_servertime_${formatVersion}`;
const storageKeyColor = `isleward_addon_timestamp_color_${formatVersion}`;
const defaultFormat = '[hh&#058;mm&#058;ss a]';
const defaultServerTime = false;
const defaultColor = 'grayD';
const allowedColors = [
	'q0',
	'q1',
	'q2',
	'q3',
	'q4',
	'red',
	'redA',
	'blueA',
	'blueB',
	'greenB',
	'yellowB',
	'green',
	'brownC',
	'brownD',
	'grayA',
	'grayB',
	'grayC',
	'grayD',
	'pinkA',
	'pinkB',
	'purpleA',
	'tealB',
];

let _userFormat = null;
function getFormat() {
	if (_userFormat === null) {
		_userFormat = localStorage.getItem(storageKeyFormat) ?? defaultFormat;
	}
	return _userFormat;
}
function setFormat(value) {
	localStorage.setItem(storageKeyFormat, value);
	_userFormat = value;
}

let _useServerTime = null;
function getUseServerTime() {
	if (_useServerTime === null) {
		_useServerTime =
			localStorage.getItem(storageKeyServerTime) ?? defaultServerTime;
	}
	return _useServerTime;
}
function setUseServerTime(value) {
	localStorage.setItem(storageKeyServerTime, value);
	_useServerTime = value;
}

let _userColor = null;
function getColor() {
	if (_userColor === null) {
		_userColor = localStorage.getItem(storageKeyColor) ?? defaultColor;
	}
	return _userColor;
}
function setColor(value) {
	localStorage.setItem(storageKeyColor, value);
	_userColor = value;
}

function wrapWithColor(text) {
	const color = getColor();

	// Don't wrap with color
	if (color === '') return text;

	let props = '';
	if (color.startsWith('#')) {
		props = `style="color: ${color};"`;
	} else {
		props = `class="${color}"`;
	}

	return `<span ${props}>${text}</span>`;
}

function formattedNow() {
	const userFormat = getFormat();

	const time = getUseServerTime()
		? utcToZonedTime(new Date(), 'UTC')
		: new Date();
	const opts = getUseServerTime() ? { timeZone: 'UTC' } : undefined;

	try {
		const formatted = format(time, userFormat, opts);

		return wrapWithColor(formatted);
	} catch (e) {
		setFormat(defaultFormat);

		console.log(`Bad timestamp format: ${userFormat}, reset to default`);

		sendMessage(
			'Invalid timestamp format! Resetting to default.',
			'color-yellowB',
			'info'
		);

		return formattedNow();
	}
}

function onBeforeChat(msgConfig) {
	if (msgConfig.message.startsWith('/help')) {
		sendMessage(
			'/timeformat &lt;format&gt;<br />/timecolor &lt;color&gt;<br />/toggletimezone',
			'color-yellowB',
			'info'
		);
	} else if (msgConfig.message.startsWith('/timecolor')) {
		msgConfig.success = false;

		// Validate
		let color = msgConfig.message.replace(/^\s*\/timecolor\s*/, '');

		if (color.length < 1) {
			setColor('');

			sendMessage(
				`Cleared color, preview: ${formattedNow()}`,
				'color-yellowB',
				'info'
			);

			return;
		}

		console.log(msgConfig, color);

		if (!allowedColors.includes(color) && !color.startsWith('#')) {
			sendMessage(
				`Provide a hex code or use: ${allowedColors.join(', ')}`
			);
			return;
		}

		if (!color.startsWith('q') && !color.startsWith('#')) {
			color = `color-${color}`;
		}

		setColor(color);

		sendMessage(
			`Updated color, preview: ${formattedNow()}`,
			'color-yellowB',
			'info'
		);
	} else if (msgConfig.message.startsWith('/timeformat')) {
		msgConfig.success = false;

		// Sanitize
		const newFormat = msgConfig.message
			.replace(/^\s*\/timeformat\s*/, '')
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#039;')
			.replace(/:/g, '&#058;'); // IWD chat does something to linked-item messages with colons

		if (newFormat.length < 1) {
			sendMessage(
				'Usage: /timeformat &lt;format&gt;',
				'color-redA',
				'info'
			);
			return;
		}

		setFormat(newFormat);

		sendMessage(
			`Updated format, preview: ${formattedNow()}`,
			'color-yellowB',
			'info'
		);
	} else if (msgConfig.message.startsWith('/toggletimezone')) {
		msgConfig.success = false;

		setUseServerTime(!getUseServerTime());

		sendMessage(
			`Timestamps use server time: ${getUseServerTime()}`,
			'color-yellowB',
			'info'
		);
		sendMessage(
			`Update timezone, preview: ${formattedNow()}`,
			'color-yellowB',
			'info'
		);
	}
}

function onGetMessages(obj) {
	if (obj.messages) {
		if (!Array.isArray(obj.messages)) {
			obj.messages = [obj.messages];
		}

		for (let i = 0; i < obj.messages.length; i++) {
			const m = obj.messages[i].message;
			if (m) {
				obj.messages[i].message = `${formattedNow()} ${m}`;
			}
		}
	}
}

function sendMessage(message, color, type = 'info') {
	window.addons.events.emit('onGetMessages', {
		messages: [
			{
				class: color,
				message: message,
				type: type,
			},
		],
	});
}

// prettier-ignore
(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'timestamps' : ADDON_ID,
	name: 'Timestamps',

	init: function () {
		console.log('[Timestamps]', `Format is: ${getFormat()}, color is: ${getColor()}`);
		console.log('[Timestamps]', `Preview: ${formattedNow()}`);

		window.addons.events.on('onBeforeChat', onBeforeChat);
		window.addons.events.on('onGetMessages', onGetMessages);
	},

	// Can't be hot-reloaded because the onGetMessages listener needs to be registered before the IWD chat's listener
	// cleanup: function () {
	// window.addons.events.off('onBeforeChat', onBeforeChat);
	// window.addons.events.off('onGetMessages', onGetMessages);
	// },
});
