const path = require('path');
const url = require('url');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackUserscript = require('webpack-userscript');

const isDev = process.env.NODE_ENV !== 'production';
const pkg = require('./package.json');

const devBaseUrl = path.resolve(__dirname, 'build');
const devBaseUrlFile = url.pathToFileURL(devBaseUrl).href;

// Change these to whatever
const ADDON_ID = 'timestamps';
const ADDON_NAME = 'Timestamps';
const ADDON_AUTHOR = 'kckckc';
const ADDON_NAMESPACE = 'Isleward.Addon'; // ?
const ADDON_DESCRIPTION = `${ADDON_NAME} addon for Isleward.`;
const ADDON_GRANT = 'none';
const ADDON_MATCH = [
	'http*://play.isleward.com/*',
	'http*://ptr.isleward.com/*',
];

const DOWNLOAD_BASE_URL =
	'https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/timestamps/dist/timestamps.user.js'; // Link to latest hosted [addon].user.js
const UPDATE_BASE_URL =
	'https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/timestamps/dist/timestamps.meta.js'; // Link to latest hosted [addon].meta.js

module.exports = (env) => ({
	mode: isDev ? 'development' : 'production',
	entry: path.resolve(__dirname, pkg.main),
	output: {
		path: path.resolve(__dirname, env.BUILD_DIR ?? 'build'),
		filename: `${ADDON_ID}.user.js`,
	},
	devtool: isDev ? 'inline-source-map' : false,
	devServer: {
		port: 8080,
		static: {
			directory: devBaseUrl,
		},
		devMiddleware: {
			writeToDisk: true,
		},
		headers: {
			'Access-Control-Allow-Origin': '*',
		},

		// Doesn't really work with userscripts
		hot: false,
		liveReload: false,
		client: false,
	},
	optimization: {
		minimizer: [new TerserPlugin({ extractComments: false })],
		minimize: !isDev,
	},
	plugins: [
		new WebpackUserscript({
			headers: {
				name: ADDON_NAME,
				namespace: ADDON_NAMESPACE,
				version: isDev ? `[version]-build.[buildNo]` : `[version]`,
				description: ADDON_DESCRIPTION,
				author: ADDON_AUTHOR,
				grant: ADDON_GRANT,
				match: ADDON_MATCH,
			},

			downloadBaseUrl: DOWNLOAD_BASE_URL,
			updateBaseUrl: UPDATE_BASE_URL,

			proxyScript: {
				baseUrl: devBaseUrlFile,
				filename: '[basename].proxy.user.js',
				enable: () => env.ADDON_PROXY === '1',
			},
		}),
	],
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	module: {
		rules: [
			{
				test: /\.(less|css)$/i,
				use: ['style-loader', 'css-loader', 'less-loader'],
			},
		],
	},
});
