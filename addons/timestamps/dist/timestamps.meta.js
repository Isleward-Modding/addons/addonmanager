// ==UserScript==
// @name        Timestamps
// @version     1.2.2
// @author      kckckc
// @description Timestamps addon for Isleward.
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// @grant       none
// @downloadURL https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/timestamps/dist/timestamps.user.js
// @updateURL   https://gitlab.com/Isleward-Modding/addons/addonmanager/-/raw/master/addons/timestamps/dist/timestamps.meta.js
// ==/UserScript==
