// Basic Addon Example
// You should also include a userscript header for compatibility

// We could do something like this if we are bundling using Webpack:
// import { mergeWith } from 'lodash';

// We can keep state here, outside the addon object
let currentMap;

// We can also keep our handlers outside the addon object, since managing `this` is a hassle
function onGetMap(map) {
	console.log('Got map!');

	currentMap = map;
}

(a=>{let p=_=>{let m=(typeof unsafeWindow==='undefined'?globalThis:unsafeWindow)[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	// Used for logging and hot-reloading. ID should be unique
	id: typeof ADDON_ID === 'undefined' ? 'example-addonmanager-addon' : ADDON_ID,
	name: 'Example AddonManager Addon',

	// Note: If we use an arrow function, we can't access `this`
	init: () => {
		console.log('Loaded addon!');

		// window.addons.events is guaranteed to be available now
		window.addons.events.on('onGetMap', onGetMap);
	},

	cleanup: () => {
		// If you don't want/need to support hot-reload, remove this function from the addon object
		console.log('Cleanup using AddonManager!');

		// Remove listeners!!!
		// This is important so that old listeners are not still firing after a new version of the addon was loaded
		window.addons.events.off('onGetMap', onGetMap);
	}
});
